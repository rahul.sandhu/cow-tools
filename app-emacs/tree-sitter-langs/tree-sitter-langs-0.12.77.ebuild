# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit elisp

DESCRIPTION="https://emacs-tree-sitter.github.io/languages/"
HOMEPAGE="https://emacs-tree-sitter.github.io/languages/
	https://github.com/emacs-tree-sitter/tree-sitter-langs"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/emacs-tree-sitter/${PN}.git"
else
	SRC_URI="https://github.com/emacs-tree-sitter/${PN}/archive/refs/tags/${PV}.tar.gz
		-> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

LICENSE="GPL-3+"
SLOT="0"

DOCS=( README.md )
SITEFILE="50${PN}-gentoo.el"

RDEPEND="
	dev-libs/tree-sitter
"
BDEPEND="${RDEPEND}"

elisp-enable-tests ert test

src_compile() {
	elisp_src_compile
	elisp-make-autoload-file
}
