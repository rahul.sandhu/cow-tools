# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop xdg-utils

DESCRIPTION="Wire desktop client"
HOMEPAGE="https://wire.com/"
SRC_URI="https://github.com/wireapp/wire-desktop/releases/download/linux/${PV}/Wire-${PV}_amd64.deb"

LICENSE="GPL-3 MIT BSD"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}

src_unpack() {
	default
	unpack ./data.tar.xz
	rm data.tar.xz control.tar.gz debian-binary
}

src_install() {
	domenu usr/share/applications/wire-desktop.desktop
	doicon --size 32 usr/share/icons/hicolor/32x32/apps/wire-desktop.png
	doicon --size 256 usr/share/icons/hicolor/256x256/apps/wire-desktop.png
	dodoc -r usr/share/doc/wire-desktop/*
	insinto /opt
	doins -r opt/Wire
	dosym /opt/Wire/wire-desktop /usr/bin/wire-desktop

	docompress -x /usr/share/doc/${P}/changelog.gz
	fperms 0755 /opt/Wire/wire-desktop
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
