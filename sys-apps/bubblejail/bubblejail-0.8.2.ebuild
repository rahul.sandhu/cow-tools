# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson xdg

DESCRIPTION="Bubblewrap based sandboxing for desktop applications"
HOMEPAGE="https://github.com/igo95862/bubblejail"
SRC_URI="https://github.com/igo95862/${PN}/releases/download/${PV}/${PN}-${PV}.tar.gz"
S="${WORKDIR}"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	>=dev-lang/python-3.10
	dev-python/PyQt6
	dev-python/pyxdg
	dev-python/tomli
	dev-python/tomli-w
	sys-apps/xdg-dbus-proxy
	>=sys-apps/bubblewrap-0.5.0
	sys-libs/libseccomp
"
DEPEND="
	${RDEPEND}
"
BDEPEND="
	sys-devel/bison
"

src_configure() {
	local emesonargs=(
		-Dman=true
	)
	meson_src_configure
}

pkg_postinst() {
	xdg_icon_cache_update
}
